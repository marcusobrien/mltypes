# README #

### What is this repository for? ###

* Its a collection of examples for machine learning. The idea is to KISS. So each is a simple decoupled python script, that can be
  executed. The data is included in the data folder, all data has been downloaded and saved locally to avoid stale URLs.

* Version - no versioning except repo hashes
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Setup - Its better when using python to not have an installed Uber python, that will be used in your path by everything. Its too hard to keep all the packages compatible. So using conda (Ana or mini) allows you
to have lots of pythons, each with their own version and specific versions of packaged. You only need 1 such environment for all these tutorials, but creating one for them will keep them working, and validate a stable environment for your own ml work.  Install anaconda or miniconda - windows or linux, or wsl etc. Then create a conda environment

* Configuration - Use the list files in this repo to see the versions of the packages you need, or see below.

*Add to that environment the following
*python 3.8.5
*numpy 1.18.5
*scikit-learn 0.23.1
*pandas 1.0.5
*tensorflow 2.3.1
*matplotlib 3.3.2

* Dependencies - Use CUDA dlls if you want GPU hardware acceleration. You will need the following versions
* NVidia driver : 460.79
* CUDA SDK : 10.1 update 2 - Aug 2019
* https://developer.nvidia.com/cuda-toolkit-archive
* cudNN : 7.6.5 - Nov 5th 219
* https://developer.nvidia.com/rdp/cudnn-archive
* cudnn is not just unzipping - need to copy 3 files around so follow guide for that

*All detailed here
*https://shawnhymel.com/1961/how-to-install-tensorflow-with-gpu-support-on-windows/

*Now using tensorflow should show the GPU being used. See the GPU usage on a tool like GPU-Z

* Database configuration - None

* Run each script on its own through either python script_name using a conda environment - activate conda_env, then run python script_name, or through an IDE such as pycharm (use the installer with anaconda
plugin), or Visual Studio code etc.

### Contribution guidelines ###

### Who do I talk to? ###

* Repo owner or admin
marcusobrien@yahoo.com

