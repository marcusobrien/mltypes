from sklearn import tree
#import  matplotlib.pyplot as plt
import seaborn as sb

# decision tree
# labels
# orange = 0
# apple = 1

# features
# rough
# smooth

features = [[155, 0], [180, 0], [135, 1], [110, 1]] # scikit-learn requires real-valued features

# labels = [“orange”, “orange”, “apple”, “apple”] # output values
labels = [1, 1, 0, 0]

#display data

# Training classifier
classifier = tree.DecisionTreeClassifier() # using decision tree classifier
classifier = classifier.fit(features, labels) # Find patterns in data

# Making predictions
prediction = classifier.predict([[120, 1]])

if prediction == 0:
    fruit = "orange"
else:
    fruit = "apple"

print("Using texture as 120, the Prediction is {} based on value of {}".format(fruit, prediction))
# Output is 0 for apple
