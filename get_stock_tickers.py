import os

verbose = False

def getListOfStockTickers():
    os.chdir(r'X:\nasdaq')
    if verbose:
        print('Current folder : {} '.format(os.getcwd()))
    nasdaq_tickers = []

    with open('top_nasdaq_tickers.txt') as file:
        lines = file.read()
        line_list = lines.split('\n')
        counter = 0
        for data in line_list:
            counter += 1
            ticker = data.split('\t')
            if not ticker or len(ticker[0]) < 1:
                if verbose:
                    print("Data was invalid.")
            else:
                if verbose:
                    print('({}) : {}'.format(counter, ticker))
                nasdaq_tickers.append(ticker[0])
    return nasdaq_tickers

nasdaq_list=getListOfStockTickers()
print("NASDAQ top {} stock tickers : {} ".format(len(nasdaq_list), nasdaq_list))
