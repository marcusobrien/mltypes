import numpy as np
import pandas as pd

# importing the model
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split

from sklearn import metrics

def theta_calculate(x_train, y_train):
    n_data = x_train.shape[0]
    bias = np.ones((n_data,1))
    x_train_b = np.append(bias, x_train, axis=1)

    theta_1 = np.linalg.inv(np.dot(x_train_b.T, x_train_b))
    theta_2 = np.dot(theta_1, x_train_b.T)
    theta = np.dot(theta_2, y_train)
    print('theta:{}'.format(theta))
    return theta

def setupData():
    data_path = r"D:\Projects19\repos\mine\mltypes\data\Advertising.csv" # loading the advertising dataset
    data = pd.read_csv(data_path, index_col=0)
    array_items = ['TV', 'Radio', 'Newspaper'] #creating an array list of the items

    X = data[array_items] #choosing a subset of the dataset
    print("Training Input Values \n{}".format(X))

    y = data.Sales #sales
    print("Training Output - Sales Values \n{}".format(y))

    # dividing X and y into training and testing units
    X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=1)
    print('Split Training Data into X input Size {} and Y output Size {}'.format(len(X_train), len(y_train)))
    print('Split Test Data into X input Size {} and Y output Size {}'.format(len(X_test), len(y_test)))
    return X_train, X_test, y_train, y_test


X_train, X_test, y_train, y_test = setupData()

linearreg = LinearRegression() #applying the linear regression model

print('Prefit - Created linear regression:{}'.format(linearreg))
linearreg.fit(X_train, y_train) #fitting the model to the training data

print('Postfit - Created linear regression:{}'.format(linearreg))

y_predict = linearreg.predict(X_test) #making predictions based on the testing unit
print('Y Prediction: {}'.format(y_predict))

print("RMSE:{}".format(np.sqrt(metrics.mean_squared_error(y_test, y_predict)))) #calculating the RMSE number

theta_calculate(x_train =X_train, y_train=y_train)
