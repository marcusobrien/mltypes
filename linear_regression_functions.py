import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from pandas.plotting import scatter_matrix
from sklearn.model_selection import train_test_split

#initial MSE
def mse_calc(slope, intercept, d_pt):
    tot_error = 0
    pred = []
    for i in range(len(d_pt)):
        x = d_pt[i,0]
        y = d_pt[i,1]
        #calculating total error. It follows the formula y=mx+c
        #m is the slope and c is the intercept
        pred.append(((slope*x) + intercept))
        tot_error += (y - ((slope*x) + intercept)) ** 2
    error = tot_error / float(len(d_pt))
    return error, pred


# gradient descent
def grad_descent(s_slope, s_intercept, l_rate, iter_val, d_pt) :
    for i in range (iter_val) :
        int_slope = 0
        int_intercept = 0
        n_pt = float (len (d_pt))

        for i in range (len (d_pt)) :
            x = d_pt[i, 0]
            y = d_pt[i, 1]
            int_intercept += - (2 / n_pt) * (y - ((s_slope * x) + s_intercept))
            int_slope += - (2 / n_pt) * x * (y - ((s_slope * x) + s_intercept))

        final_slope = s_slope - (l_rate * int_slope)
        final_intercept = s_intercept - (l_rate * int_intercept)
        s_slope = final_slope
        s_intercept = final_intercept

    return s_slope, s_intercept

def setupData():
    data_path = r"D:\Projects19\repos\mine\mltypes\data\Advertising.csv" # loading the advertising dataset
    data = pd.read_csv(data_path, index_col=0)
    X = data.TV.values
    print("Training Input Values \n{}".format(X))

    y = data.Radio.values
    print("Training Output - Sales Values \n{}".format(y))

    # dividing X and y into training and testing units
    X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=1)
    print('Split Training Data into X input Size {} and Y output Size {}'.format(len(X_train), len(y_train)))
    print('Split Test Data into X input Size {} and Y output Size {}'.format(len(X_test), len(y_test)))
    return X_train, X_test, y_train, y_test


#defining slope and intercept value as 0
learning_rate = 0.0001
start_slope = 0
start_intercept = 0
iteration = 950

X_train, X_test, y_train, y_test = setupData()
input_val = X_train
output_val = y_train

#cant get this to work
data_pt = np.ndarray((2,len(input_val)))

data_pt[0] = input_val
data_pt[1] = output_val

#initial run

e_value, prediction = mse_calc(start_slope, start_intercept, data_pt)
print('initial error', e_value)

#Graph
plt.scatter(input_val, output_val)
plt.plot(input_val[0], prediction, color='blue', linewidth = 3)
plt.xlabel("Input")
plt.ylabel("Output")
plt.show()
