# Example from machine learning - average etc
# https://www.analyticsvidhya.com/blog/2018/10/predicting-stock-price-machine-learningnd-deep-learning-techniques-python/?

#                                           Understanding the problem

# import packages
import pandas as pd
import numpy as np

from sklearn import neighbors
from sklearn.model_selection import GridSearchCV


pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)


import matplotlib.pyplot as plt
from matplotlib.pylab import rcParams

rcParams['figure.figsize'] = 20,10


from sklearn.preprocessing import MinMaxScaler
scaler = MinMaxScaler(feature_range=(0, 1))

# read the file
df = pd.read_csv(r'D:\Projects19\repos\mine\mltypes\data\RawData_AMGN_2020-12-11.csv')

print('\n Data read in from csv. Shape of the data: {} \n Head of data {}'.format(df.shape,df.head()))

#                                           END OF SECTION 1

# setting index as date
df['Date'] = pd.to_datetime(df.Date,format='%Y-%m-%d')
df.index = df['Date']

# plot - Number 1
fig,(ax1,ax2,ax3,ax4) = plt.subplots(4,1)
fig.suptitle("Closing prices")

ax1.plot(df['Close'], 'g', label='All Actual Closing Price')
ax1.set_ylabel('All Actual Closing Price')

#                                           End of 2nd Section

# END OF Acquire, Clean, PLOTTING CLOSING PRICES

#                                   Moving Average

# creating dataframe with date and the target variable
data = df.sort_index(ascending=True, axis=0)
new_data = pd.DataFrame(index=range(0,len(df)),columns=['Date', 'Close'])

for i in range(0,len(data)):
     new_data['Date'][i] = data['Date'][i]
     new_data['Close'][i] = data['Close'][i]

# NOTE: While splitting the data into train and validation set, we cannot use random splitting since
# that will destroy the time component. So here we have set the last year’s data into validation and
# the 4 years’ data before that into train set.

train = df['1990-01-10':'2015-12-31']
valid = df['2016-01-01':]

print('\n Training Data - Shape of the data: {} \n Head of data {}'.format(train.shape,train.head()))

print('\n Validation Data - Shape of the data: {} \n Head of data {}'.format(valid.shape,valid.head()))

preds = []
valid_size = len(valid)

for i in range(0,valid_size):
    a = train['Close'][len(train)-valid_size+i:].sum() + sum(preds)
    b = a/valid_size
    preds.append(b)

# checking the results (RMSE value)
rms=np.sqrt(np.mean(np.power((np.array(valid['Close'])-preds),2)))
print('\n RMSE value on validation set:')
print(rms)
# Inference - RSME is low at 33.495897 - but the prediction in the graph doesnt look much like the actual price on the graph

#plot - 2
valid['Predictions'] = preds
ax2.plot(train['Close'], 'b', label='Training Actual Close')
ax2.plot(valid['Close'], 'g', label='Validation Actual Close')
ax2.plot(valid['Predictions'],  'r', label='Average Predicted Close')
ax2.set_ylabel('Moving Average Close')

#               Inference
#                                                END OF SECTION 3

#                               Linear Regression

#Implementation

#setting index as date values
df['Date'] = pd.to_datetime(df.Date,format='%Y-%m-%d')
df.index = df['Date']

#sorting
data = df.sort_index(ascending=True, axis=0)

#creating a separate dataset
new_data = pd.DataFrame(index=range(0,len(df)),columns=['Date', 'Close'])

for i in range(0,len(data)):
    new_data['Date'][i] = data['Date'][i]
    new_data['Close'][i] = data['Close'][i]

#create features

from fastai.tabular.core import add_datepart
add_datepart(new_data, 'Date')
new_data.drop('Elapsed', axis=1, inplace=True)  #elapsed will be the time stamp

new_data['mon_fri'] = 0

for i in range(0,len(new_data)):
    if (new_data['Dayofweek'][i] == 0 or new_data['Dayofweek'][i] == 4):
        new_data.loc[i:i+1, ('mon_fri')]=1
    else:
        new_data.loc[i:i+1, ('mon_fri')]=0

print("new_data with features added via fastai :{}".format(new_data))
SIZE_TRAINING_DATA = 6000

#split into train and validation
train = new_data[:SIZE_TRAINING_DATA]
valid = new_data[SIZE_TRAINING_DATA:]

x_train = train.drop('Close', axis=1)
print('x_train head {}\n'.format(x_train.head(10)))
y_train = train['Close']
print('y_train head {}\n'.format(y_train.head(10)))
x_valid = valid.drop('Close', axis=1)
print('x_valid head {}\n'.format(x_valid.head(10)))
y_valid = valid['Close']
print('y_valid head {}\n'.format(y_valid.head(10)))

#implement linear regression
from sklearn.linear_model import LinearRegression
model = LinearRegression()
model.fit(x_train,y_train)

#                   RESULTS

#make predictions and find the rmse
preds = model.predict(x_valid)


#                   RSME
rms=np.sqrt(np.mean(np.power((np.array(y_valid)-np.array(preds)),2)))

print('rms:{}'.format(rms))

valid.insert(14, "Predictions", preds, True)

valid.index = new_data[SIZE_TRAINING_DATA:].index
train.index = new_data[:SIZE_TRAINING_DATA].index

ax3.plot(train['Close'], 'b', label='Training Actual Close')
ax3.plot(valid['Close'], 'g', label='Validation Actual Close')
ax3.plot(valid['Predictions'],  'r', label='Linear Regression Predicted Close')
ax3.set_ylabel('Linear Prediction Close')

#               kNN Nearest Neighbour

#scaling data
x_train_scaled = scaler.fit_transform(x_train)
x_train = pd.DataFrame(x_train_scaled)
x_valid_scaled = scaler.fit_transform(x_valid)
x_valid = pd.DataFrame(x_valid_scaled)

#using gridsearch to find the best parameter
params = {'n_neighbors':[2,3,4,5,6,7,8,9]}
knn = neighbors.KNeighborsRegressor()
model = GridSearchCV(knn, params, cv=5)

#fit the model and make predictions
model.fit(x_train,y_train)
preds = model.predict(x_valid)

rms=np.sqrt(np.mean(np.power((np.array(y_valid)-np.array(preds)),2)))
rms

valid2 = new_data[SIZE_TRAINING_DATA:]
valid2.insert(14, "Predictions", preds, True)
ax4.plot(train['Close'], 'b', label='Training Actual Close')
ax4.plot(valid2['Close'], 'g', label='Validation Actual Close')
ax4.plot(valid2['Predictions'],  'r', label='Average Predicted Close')
ax4.set_ylabel('kNN Nearest Neighbour')

plt.show(block=True)

print("End")


