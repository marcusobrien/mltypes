import pandas as pd
import numpy as np

#Create a dataframe with dates as index, 8 rows, 4 columns, and random data
def createDataFrameColsRowsIndex(rows, cols):
    cs = []
    for a in range(0, cols, 1):
        cs.append(chr(a+ord('A')))
    dates = pd.date_range('1/1/2000', periods=rows)
    df = pd.DataFrame(np.random.randn(rows, cols),index=dates, columns=cs)
    return df

df = createDataFrameColsRowsIndex(8,4)
print(df)
